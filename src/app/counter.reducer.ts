import { createReducer, on } from '@ngrx/store';
import { Action } from 'rxjs/internal/scheduler/Action';
import { increment, decrement, reset } from './counter.actions';

export const initialState = 0;

export function counterReducer(state = initialState, action) { //this state is the current state and action is the triggered manipulation action
  console.log(action.type)
  switch(action.type){
    case 'Increment': {
      return state+=1;
    }
    case 'Decrement': {
      return state-=1;
    }
    case 'Reset': {
      return state=0;
    }

    default: return state;
  }
}




















/*

// const _counterReducer = createReducer(
//   initialState,
//   on(increment, (state) => state + 1),
//   on(decrement, (state) => state - 1),
//   on(reset, (state) => 0)
// );
const _counterReducer = createReducer(
  initialState,
  on(increment, (state) => state + 1),
  on(decrement, (state) => state - 1),
  on(reset, (state) => 0)
);

export function counterReducer(state = initialState, action) { //this state is the current state and action is the triggered manipulation action
  return _counterReducer(state, action);
}

*/